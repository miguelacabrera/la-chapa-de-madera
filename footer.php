<section class="emergency-contact">
    <div class="left-side">
        <div class="content">
            <h3>Solicita el tipo de madera que necesitas</h3>
            <h3>Estamos para servirte</h3>
            <a href="tel:(442) 2241-555" class="phone"><img src="/assets/images/great-work/phone.png" alt="">(442) 2241-555</a>
            <a href="mailto:ventalachapa@gmail.com" class="email"><img src="/assets/images/great-work/email.png" alt="">ventalachapa@gmail.com</a>
        </div>
</section>

<footer class="row">
    
     <div class="row m0 footer-bottom">
         <div class="container">
            <div class="row">
               <div class="col-sm-8">
                   Copyright &copy; <a href="/">La ChaPa</a> 2017. <br class="visible-xs"> Todos los derechos reservados.
               </div>
               <div class="right col-sm-4">
                   Estrategia creada por: <a target="_blank" href="https://www.signe360.com/estrategias-para-empresas.php">Signe 360</a>
               </div>
            </div>
        </div>
     </div>
</footer>
<?PHP require_once("scripts_util.php"); ?>



<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Chapas de madera ahumadas</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body class="st">
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Chapas ahumadas</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Chapas ahumadas</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/alerce-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Alerce" data-desc="Alerce" class="popup">
                        <img src="/assets/chapas/ahumadas/alerce-chapa-madera-ahumada-320x530.jpg" alt="Alerce">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Alerce</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/roble-fume-oscuro-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Roble Fume Oscuro" data-desc="Roble Fume Oscuro" class="popup">
                        <img src="/assets/chapas/ahumadas/roble-fume-oscuro-madera-ahumada-320x530.jpg" alt="Fume">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Fume Oscuro</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/roble-fume-marron-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Roble Fume Marrón" data-desc="Roble Fume Marrón" class="popup">
                        <img src="/assets/chapas/ahumadas/roble-fume-marron-chapa-madera-ahumada-320x530.jpg" alt="Roble">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Fume Marrón</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/nogal-satin-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Nogal Satín" data-desc="Nogal Satín" class="popup">
                        <img src="/assets/chapas/ahumadas/nogal-satin-chapa-madera-ahumada-320x530.jpg" alt="Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Satín</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/eucalipto-fume-flama-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Eucalipto Fumé Flama" data-desc="Eucalipto Fumé Flama" class="popup">
                        <img src="/assets/chapas/ahumadas/eucalipto-fume-flama-chapa-madera-ahumada-320x530.jpg" alt="Eucalipto">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Eucalipto Fumé Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/eucalipto-fume-rayado-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Eucalipto Fume Rayado" data-desc="Eucalipto Fume Rayado" class="popup">
                        <img src="/assets/chapas/ahumadas/eucalipto-fume-rayado-chapa-madera-ahumada-320x530.jpg" alt="Eucalipto">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Eucalipto Fume Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/eucalipto-friseado-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Eucalipto Friseado" data-desc="Eucalipto Friseado" class="popup">
                        <img src="/assets/chapas/ahumadas/eucalipto-friseado-chapa-madera-ahumada-320x530.jpg" alt="Eucalipto">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Eucalipto Friseado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/sapelly-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Sapelly" data-desc="Sapelly" class="popup">
                        <img src="/assets/chapas/ahumadas/sapelly-chapa-madera-ahumada-320x530.jpg" alt="Sapelly">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sapelly</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/moonring-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Moonring" data-desc="Moonring" class="popup">
                        <img src="/assets/chapas/ahumadas/moonring-chapa-madera-ahumada-320x530.jpg" alt="Moonring">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Moonring</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/dark-mahogany-chapa-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Dark Mahogany" data-desc="Dark Mahogany" class="popup">
                        <img src="/assets/chapas/ahumadas/dark-mahogany-chapa-madera-ahumada-320x530.jpg" alt="Mahogany">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Dark Mahogany</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            
            


        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>
</body>
</html>
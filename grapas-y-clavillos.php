<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Grapas y clavillos</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body>
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Grapas y clavillos</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li class="active">Grapas y clavillos</li>
            </ol>
            </div>
        </div>
</section>

<section class="row sectpad services">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 sidebar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="enchapados">
                            Enchapados
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="chapacinta">
                           Chapacinta
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="formaicas">
                            Formaicas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="planchones-y-rodajas">
                            Planchones y rodajas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="raices-y-rarezas">
                            Raíces y Rarezas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aceite-y-vida-madera">
                            Aceite y vida madera
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="engrapadora-y-clavadora">
                            Engrapadora y clavadora
                       </a>
                    </li>
                    <li role="presentation">
                        <a href="grapas-y-clavillos">
                            Grapas y clavillos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="corcho">
                            Corcho
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="pegamentos">
                            Pegamentos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="herramienta">
                            Herramienta de carpintería
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="playo-hule-burbuja-polifon">
                            Playo, hule burbuja y polifón
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aluminio">
                            Aluminio
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="content-1">
                        <div class="row m0 tab_inn_cont_1 ">
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-8">
                                    <div class="row m0 section_header color">
                                        <h2>Grapas y clavillos</h2> 
                                    </div>
                                    <p>Disponemos de todas las medidas de grapas y clavilos FIFA&reg;</p>
                                </div>
                                <div class="cont_right col-sm-4">
                                    <img src="/assets/images/productos/grapas/grapas-chapasdemadera1.jpg" alt="grapas">                                
                                </div>
                            </div>
                            <div class="col-md-12">
                                <img src="/assets/images/productos/grapas/grapas-chapasdemadera2.jpg" alt="chapasdemadera">
                            </div>
                            <div class="col-md-12">
                                <img src="/assets/images/productos/grapas/grapas-chapasdemadera3.jpg" alt="productos chapasdemadera">
                            </div>
                            <div class="col-md-12">
                                <img src="/assets/images/productos/grapas/grapas-chapasdemadera4.jpg" alt="productos">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                     
        </div>
    </div>
</section>

<?PHP require_once("footer.php"); ?>
</body>
</html>
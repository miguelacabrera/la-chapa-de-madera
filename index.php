<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>La ChaPa - Chapas de madera exóticas</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body>
<?PHP require_once("header.php"); ?>
<section class="bannercontainer row">
    <div class="rev_slider banner row m0" id="rev_slider" data-version="5.0">
        <ul>
            <li data-transition="slidehorizontal"  data-delay="10000">
                <img src="/assets/images/slider-img/banner1-chapas-madera-lachapa.jpg"  alt="chapas" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="img-responsive">
            </li>
            
            <li data-transition="parallaxvertical">
                <img src="/assets/images/slider-img/banner2-chapas-madera-lachapa.jpg"  alt="chapas-madera" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="img-responsive">
      
            </li>
             <li data-transition="parallaxvertical">
                <img src="/assets/images/slider-img/banner3-chapas-madera-lachapa.jpg"  alt="lachapa" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="img-responsive">
            </li>
             <li data-transition="parallaxvertical">
                <img src="/assets/images/slider-img/banner4-chapas-madera-lachapa.jpg"  alt="chapa" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="img-responsive">
            </li>
            </li>
             <li data-transition="parallaxvertical">
                <img src="/assets/images/slider-img/banner5-chapas-madera-lachapa.jpg"  alt="chapas madera" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="img-responsive">
            </li>
        </ul>
    </div>
</section>

<section class="row experience-area">
   <div class="container">
       <div class="row">
           <div class="col-sm-4 worker-image">
               <img src="/assets/images/expreence/chapas-madera-exotica-lachapa.png" alt="madera exotica">
           </div>
           <div class="col-sm-4 experience-info">
              <div class="content">
                  <h2>LA CHAPA</h2> 
                  <p>Nuestro material ayuda reducir la tala moderada de árboles, preservar nuestros bosques y mejor aprovechamiento.</p>  
                  <p>Las chapas naturales de madera son una delgada lámina generalmente utilizada en cortes menores a 3mm. Su aplicación mejora el diseño utilizando colores naturales, textura, logrando un diseño único.</p> 
                  <p>En La ChaPa manejamos amplia variedad de chapas naturales para satisfacer tu diseño, ideas e imaginación.</p>  
              </div>
           </div>
              <div class="col-sm-4 worker-image">
               <img src="/assets/images/expreence/chapas-madera-exotica-lachapa-1.jpg" alt="madera exotica">
           </div>
       </div>
   </div>
</section>

<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="row m0 section_header">
            <h2>Imagina, crea y diseña tu propio entorno</h2>
        </div>
        <div class="row m0 filter_row">

        </div>
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            <div class="col-sm-6 col-xs-6 project indoor naturales">
                <div class="project-img">
                    <a href="/assets/images/latest-project/chapas-naturales-chapas-madera-580x260.jpg" data-source="projects-details.html" title="CHAPA NATURAL" data-desc="En barra" class="popup">
                    <img src="/assets/images/latest-project/chapas-naturales-chapas-madera-580x260.jpg" alt="chapas-naturale">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0"> 
                            <h3>Chapa natural</h3>
                            <p>En barra</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor naturales renovation renovation">
                <div class="project-img">
                    <a href="/assets/images/latest-project/gris-flama-home-chapas-madera-285x260.jpg" data-source="projects-details.html" title="GRIS FLAMA" data-desc="en puerta" class="popup">
                    <img src="/assets/images/latest-project/gris-flama-home-chapas-madera-285x260.jpg" alt="gris-flama-home">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <h3>Gris flama</h3>
                            <p>En puerta</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor tintadas">
                <div class="project-img">
                    <a href="/assets/images/latest-project/muebletv-chapas-madera-285x260.jpg" data-source="projects-details.html" title="CHAPAS DE MADERA" data-desc="En mueble de tv" class="popup">
                    <img src="/assets/images/latest-project/muebletv-chapas-madera-285x260.jpg" alt="muebletv">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <h3>Chapas de madera</h3>
                            <p>En mueble de tv</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project ahumadas repairing">
                <div class="project-img">
                    <a href="/assets/images/latest-project/planchon-madera-285x260.jpg" data-source="projects-details.html" title="PLANCHÓN" data-desc="Madera" class="popup">
                    <img src="/assets/images/latest-project/planchon-madera-285x260.jpg" alt="lanchon-madera">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <h3>Planchón</h3>
                            <p>Madera</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project repairing tintadas">
                <div class="project-img">
                    <a href="/assets/images/latest-project/puertas-parota-madera-285x260.jpg" data-source="projects-details.html" title="PUERTAS DE PAROTA" data-desc="Chapas naturales" class="popup">
                    <img src="/assets/images/latest-project/puertas-parota-madera-285x260.jpg" alt="parota">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <h3>Puertas de parota</h3>
                            <p>Chapas naturales</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 project indoor texturizadas">
                <div class="project-img">
                    <a href="/assets/images/latest-project/planchon-largo-chapas-madera-584x262.jpg" data-source="projects-details.html" title="PLANCHÓN LARGO" data-desc="Madera natural" class="popup">
                    <img src="/assets/images/latest-project/planchon-largo-chapas-madera-584x262.jpg" alt="planchon">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <h3>Planchón largo</h3>
                            <p>Madera natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>

</body>
</html>
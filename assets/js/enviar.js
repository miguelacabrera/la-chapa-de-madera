//Ponte a dormir siempre y cuando tenga una peticion como promesa de uso
function sleep(ms){
    return new Promise(resolve => setTimeout(resolve, ms));
  }
  
  //Funcion asyncrona 8 segundos dormir
  async function dormir() {
    await sleep(8000);
  }
  
  
  //Documento listo con elementos cargados
   $(document).ready(function(){
   
        var idFormulario = 'contactForm';
        
        
        var Formulario = document.getElementById(idFormulario);

        var count_inputs = Formulario.getElementsByTagName("input").length;   //Cuenta la cantidad de inputs que  estan dentro del DOOM
        var inputs = [];                                                    //Array donde se almacenara la informacion de cada campo de entrada del formulario
        var inputs_opcionales = [];                                         // Array donde se guardaran la existencia de algun campo opcional
        var inputs_obligatorios = [];                                       // Array donde se guardaran la existencia de algun campo obligatorio
        var inputs_length = [];                                             // Array donde se guardaran la longitud min y max de cada campo segun sea lo declarado en las etiquetas minlength y maxlength
        
        // Recorrre de mi idFormulario todos los campos de entrada input, y almacenalos en un array ya declarado 
        $('#'+idFormulario).each(function(i) {
            var input = $(this).find('input');
            inputs.push(input);
        });
        
        //Verifica si en el formulario existe un campo tipo textarea , en caso de existir concatenalo dentro del array de <inputs>
         
        if($('#'+idFormulario).find('textarea')){
            var textarea = $('#'+idFormulario).find('textarea');
            inputs.push(textarea);
        }

        //Usar variables como posicion de arreglo
        var inputs_ = inputs[0];
    
        // Recorre los elementos inputs para almacenar las validaciones que contienen de HTML5 , con base en ellas crearlas a nivel JS
        for(var i = 0 ; i < count_inputs; i++){
            
            var inputs_attr = inputs_[i].attributes;                        //Usar variables como posicion de arreglo de elemento en curso de bucle , obtener sus atributos
            var minlenght = 0 ;                                             // Variables como default de atributos html minlength
            var maxlenght = 0 ;                                             // Variables como default de atributos html maxlength


            // ¿El atributo minlength esta declarado dentro de la etiqueta input?
            if(typeof inputs_attr.minlength !== 'undefined') {
                
                //Cambia variable default (0) por el valor de minlength=''
                minlenght = inputs_attr.minlength.nodeValue;
                
                // ¿El atributo maxlength tambien esta declarado dentro de la misma etiqueta input?
                if(typeof inputs_attr.maxlength !== 'undefined'){
                    
                    //Cambia variable default (0) por el valor de maxlength=''
                    maxlenght = inputs_attr.maxlength.nodeValue;
                    
                    // ¿Contiene el atributo requerido la misma etiqueta input?
                    if(typeof inputs_attr.required !== 'undefined') {
                        inputs_obligatorios.push([inputs_attr.name.nodeValue]); 
   
                    //En caso de no esta declararo el atributo requerido pasa a else para almacenar <input> dentro de campos opcionales
                    }else{
                        
                        // ¿Es un campo invisible que sirve solo para almacenar informacion?
                        if(inputs_attr.type.nodeValue != 'hidden'){
                            
                            //No , procede a ser guardado en campos opcionales
                            inputs_opcionales.push([inputs_attr.name.nodeValue]); 
                
                        }
                    } 

                }
            
            // No existe el atributo minlength dentro de la etiqueta input
            }else{
                
                // ¿El atributo maxlength tambien esta declarado dentro de la misma etiqueta input?
                if(typeof inputs_attr.maxlength !== 'undefined'){
                    
                    //Cambia variable default (0) por el valor de maxlength=''
                    maxlenght = inputs_attr.maxlength.nodeValue;
                    
                    // ¿Contiene el atributo requerido la misma etiqueta input?
                    if(typeof inputs_attr.required !== 'undefined') {
                        inputs_obligatorios.push([inputs_attr.name.nodeValue]); 
                    
                    //En caso de no esta declararo el atributo requerido pasa a else para almacenar <input> dentro de campos opcionales
                    }else{
                        
                        // ¿Es un campo invisible que sirve solo para almacenar informacion?
                        if(inputs_attr.type.nodeValue != 'hidden'){
                            
                            //No , procede a ser guardado en campos opcionales
                            inputs_opcionales.push([inputs_attr.name.nodeValue]); 
                            
                
                        }
                    } 
                // En caso de que no exista una delimitacion de logitud del campo 
                }else{
                    
                    // ¿Contiene el atributo requerido la misma etiqueta input?
                    if(typeof inputs_attr.required !== 'undefined') {
                        inputs_obligatorios.push([inputs_attr.name.nodeValue]); 
   
                    //En caso de no esta declararo el atributo requerido pasa a else para almacenar <input> dentro de campos opcionales
                    }else{
                        
                        // ¿Es un campo invisible que sirve solo para almacenar informacion?
                        if(inputs_attr.type.nodeValue != 'hidden'){
                            
                            //No , procede a ser guardado en campos opcionales
                            inputs_opcionales.push([inputs_attr.name.nodeValue]); 
                
                        }
                    }
                    
                    //En caso de entrar en este else, no necestas guardar campos min o max. Porque no existen, continua con el siguiente elemento
                    
                    continue;
                }
     
                
            }
            
            // Guarda el nombre del campo junto con su validacion de longitud
            inputs_length.push({name:inputs_attr.name.nodeValue, long: [{max: maxlenght, min: minlenght}] });

                
        }
        
        var count_textarea = Formulario.getElementsByTagName("textarea").length;   //Cuenta la cantidad de textarea que  estan dentro del DOOM

        // Realizar misma logica para campos textarea 
        var textarea_ = inputs[1];
        
        
        for(var i = 0 ; i < count_textarea; i++){ 
            
            var textarea_attr = textarea_[i].attributes;                    //Usar variables como posicion de arreglo de elemento en curso de bucle , obtener sus atributos
            if(typeof textarea_attr.minlength !== 'undefined') {
             
                minlenght = textarea_attr.minlength.nodeValue;
                 
                if(typeof textarea_attr.maxlength !== 'undefined'){
                     
                    maxlenght = textarea_attr.maxlength.nodeValue;
                     
                    if(typeof textarea_attr.required !== 'undefined') {
                        inputs_obligatorios.push([textarea_attr.name.nodeValue]); 
   
                    }else{
                            
                        if(textarea_attr.type.nodeValue != 'hidden'){
                            inputs_opcionales.push([textarea_attr.name.nodeValue]); 
                
                        }
                    } 

                }
            
            }else{
            
            
                if(typeof textarea_attr.maxlength !== 'undefined'){
                     
                    maxlenght = textarea_attr.maxlength.nodeValue;
                    
                    if(typeof textarea_attr.required !== 'undefined') {
                        inputs_obligatorios.push([textarea_attr.name.nodeValue]); 
   
                    }else{
                            
                        if(textarea_attr.type.nodeValue != 'hidden'){
                            inputs_opcionales.push([textarea_attr.name.nodeValue]); 
                
                        }
                    } 
                     
                }else{
                    
                    if(typeof textarea_attr.required !== 'undefined') {
                        inputs_obligatorios.push([textarea_attr.name.nodeValue]); 
   
                    }else{
                            
                        if(textarea_attr.type.nodeValue != 'hidden'){
                            inputs_opcionales.push([textarea_attr.name.nodeValue]); 
                        }
                    } 
                    
                }
     
            }

        inputs_length.push({name:textarea_attr.name.nodeValue, long: [{max: maxlenght, min: minlenght}] });
            
        }  

        
        //  console.log(inputs_opcionales);
        //    console.log(inputs_obligatorios);
          console.log(inputs_length);
        

    var btn_submit = Formulario.getElementsByTagName("button");  
    var id_btn_enviar = btn_submit.item('id').id;

   $('#'+idFormulario).submit(function(e) {
  //  $('#'+id_btn_enviar).on("click",function(e){
  
        //Invoca por default validacion == TRUE
        var validacion = true;
       
        e.preventDefault();
        
        
        //Verifica si los campos obligatorios del formulario se encuentran existentes
        if(inputs_obligatorios!== undefined){
            
            // Itera sobre los elementos obligatorios
            for (var values in inputs_obligatorios) {
                
                //Obtener el valor escrito y el id de cada etiqueta input dentro del formulario
                var val = $('input[name='+inputs_obligatorios[values][0]+']').val();
                var id =  $('input[name='+inputs_obligatorios[values][0]+']').attr('id');
                
                // En caso de que el id no este definido significa que no existe una etiqueta input con ese id por lo tanto es un text area
                if(typeof id === 'undefined'){
                    
                    val = $('textarea[name='+inputs_obligatorios[values][0]+']').val();
                    id =  $('textarea[name='+inputs_obligatorios[values][0]+']').attr('id');
                                                
                }
                
                //Realiza validacion si el campo esta vacio
                if( val == null || val.length == 0 || /^\s+$/.test(val) ) {

                    // En caso de que un elemento se halla mandado vacio, notificale al usuario que el campo es obligatorio 
                    $('#'+id).after("<p class='text-danger validacion_error'> El campo "+inputs_obligatorios[values][0]+" es obligatorio");
                    
                    //Validacion apagado , no entra en funcion ajax
                    validacion = false;
                    
                    
                    // Muestra el mensaje de error , desaparece y eliminate despues de 3.5 seg
                    setTimeout(function() { 
                        
                        $('.validacion_error').fadeOut('slow').remove();
                        
                    },3500);
                    
                }
                    
                    
                // ¿Hay elementos con delimitacion de longitud min y/o max ?
                if(inputs_length!== undefined){
                        
                    //Iteracion de dichos elementos 
                    for (var values in inputs_length) {
                        
                        //Obtener el atributo de longitudes 'long'
                        attr_long = inputs_length[values].long[0];
                        
                            //¿El campo coincide con el elemento en curso del primer for Campos obligatorios
                            if(inputs_length[values].name == id){
                                
                                // ¿El campo cumple con las condiciones longiitud menor a attr_long.min o que longitud es mayor que attr_long.max
                                if(val.trim().length < attr_long.min || val.trim().length > attr_long.max){
                                    
                                    //Validamos si el campo maximo y minimo de caracteres por ingresar son iguales (por lo tanto es un campo con longitud exacta)
                                    if(attr_long.min == attr_long.max ){
                                        
                                        // Entonces debemos indicarle al usuario que el campo llenado entro en un error de validacion 
                                        $('#'+id).after("<p class='text-danger validacion_error'> Por favor ingrese un "+inputs_length[values].name+" de " + attr_long.min  + " caracteres");
                                    
                                        
                                    }else{
                                        
                                        // Entonces debemos indicarle al usuario que el campo llenado entro en un error de validacion 
                                        $('#'+id).after("<p class='text-danger validacion_error'> El campo "+inputs_length[values].name+" debe tener minimo " +attr_long.min + " y maximo " + attr_long.max + " caracteres");
                                        
                                    }
                                    //Validacion de campo apagado , no entra en funcion ajax
                                    validacion = false;

                                     // Muestra el mensaje de error , desaparece y eliminate despues de 3.5 seg
                                    setTimeout(function() { 
                                            
                                            $('.validacion_error').fadeOut('slow').remove();
                                            
                                    },3500);      
                                } 
                                
                            } 
                            

                        }
                    
                    
                    }
            }

        }


        //Verifica si los campos opcionales del formulario se encuentran existentes y llenos
        if(inputs_opcionales!== undefined){
            
            // Itera sobre los elementos opcionales
            for (var values in inputs_opcionales) {
                
                //Obtener el valor escrito y el id de cada etiqueta input dentro del formulario
                var val = $('input[name='+inputs_opcionales[values][0]+']').val();
                var id =  $('input[name='+inputs_opcionales[values][0]+']').attr('id');
                
                // En caso de que el id no este definido significa que no existe una etiqueta input con ese id por lo tanto es un text area
                if(typeof id === 'undefined'){
                    
                    val = $('textarea[name='+inputs_opcionales[values][0]+']').val();
                    id =  $('textarea[name='+inputs_opcionales[values][0]+']').attr('id');
                                                
                }
                
                //Realiza validacion si el campo esta no esta vacio
                if( val != null || val.length != 0 || val != '' ) {

                    // ¿Hay elementos con delimitacion de longitud min y/o max ?
                    if(inputs_length!== undefined){
                            
                        //Iteracion de dichos elementos 
                        for (var values in inputs_length) {
                            
                            //Obtener el atributo de longitudes 'long'
                            attr_long = inputs_length[values].long[0];
                            
                                //¿El campo coincide con el elemento en curso del primer for Campos obligatorios
                                if(inputs_length[values].name == id){
                                    
                                    // ¿El campo cumple con las condiciones longiitud menor a attr_long.min o que longitud es mayor que attr_long.max
                                    if(val.trim().length < attr_long.min || val.trim().length > attr_long.max){
                                        
                                        //Validamos si el campo maximo y minimo de caracteres por ingresar son iguales (por lo tanto es un campo con longitud exacta)
                                        if(attr_long.min == attr_long.max ){
                                            
                                            // Entonces debemos indicarle al usuario que el campo llenado entro en un error de validacion 
                                            $('#'+id).after("<p class='text-danger validacion_error'> Por favor ingrese un "+inputs_length[values].name+" de " + attr_long.min  + " caracteres");
                                        
                                            
                                        }else{
                                            
                                            // Entonces debemos indicarle al usuario que el campo llenado entro en un error de validacion 
                                            $('#'+id).after("<p class='text-danger validacion_error'> El campo "+inputs_length[values].name+" debe tener minimo " +attr_long.min + " y maximo " + attr_long.max + " caracteres");
                                            

                                        }
                                        
                                        //Validacion de campo apagado , no entra en funcion ajax
                                        validacion = false;

                                         // Muestra el mensaje de error , desaparece y eliminate despues de 3.5 seg
                                        setTimeout(function() { 
                                                
                                                $('.validacion_error').fadeOut('slow').remove();
                                                
                                        },3500);  
                                        
                                    } 
                                    
                                } 
                                
                            }
                        
                        }

                }
                    
                    
            }

        }
        
        var inputs_length_data = '';
        for(input in inputs_length){
            
            let name_campo = inputs_length[input].name;
            let longitud_campo = inputs_length[input].long[0];
            
            
            inputs_length_data += "&"+name_campo+'-longitud'+"="+JSON.stringify(longitud_campo);
            
            

        }
        
        var inputs_opcionales_data = '&campos_opcionales=';
        for(input in inputs_opcionales){
            
            let name_campo = inputs_opcionales[input];

           inputs_opcionales_data += name_campo+',';
            
            

        }
        
        
       if(validacion){
            
            $('#response').empty().removeClass('alert-danger').removeClass('alert-success');
            grecaptcha.ready(function () {
               grecaptcha.execute('6Lft5VYeAAAAAHpSs5oij7bpamLVoKh-qtk6D9aW', { action: 'formulario_contacto08' }).then(function (token) {

    				$("#g-recaptcha-response").val(token);
    				
    		    
    			    $.ajax({
    					url: '/assets/scripts_php/envio.php',
    					type: 'POST',
    					data: $('#'+idFormulario).serialize()+inputs_length_data+inputs_opcionales_data.slice(0, -1),
    					data: $('#'+idFormulario).serialize()+inputs_opcionales_data.slice(0, -1),

    					//dataType: 'json',
    					timeout: 80000,
    					beforeSend: function(){
                                $('#'+idFormulario).hide('slow');
                                $('#response').show().html('Enviando... <i class="fa fa-spin fa-refresh" aria-hidden="true"></i>').addClass('alert-info');

                        },
    					success: function(data){
    					       console.log(data);
                                $("#response").removeClass('alert-info');
                                //var response = JSON.parse(data);
                                var response = data;
                                if(response.status == 200){
                                    
                                    $('#response').show().html(response.mensaje+' <i class="fa fa-check" aria-hidden="true"></i>').addClass('alert-success');
                                    

                                    
                                }else{
                                           

                                    if(typeof response.mensaje === 'object' && response.status == 401){
                                        
                                        $('#response').show().html( '<i class="fa fa-ban" aria-hidden="true"></i> Lo sentimos, no se pudo enviar su solicitud por los siguientes errores:').addClass('alert-danger');

                                        for(var key in response.mensaje) {
                                            if(key == 'mensaje'){
                                                $('textarea[name='+key+']').after("<p class='text-danger validacion_error'>" + response.mensaje[key]);
                                            }else{
                                                $('input[name='+key+']').after("<p class='text-danger validacion_error'>" + response.mensaje[key]);

                                            }
                                              
                                        }
                                  

                                    }else{
                                        
                                        if(response.status == 400){
                                            
                                            $('#response').show().html(response.mensaje+ ' <i class="fa fa-ban" aria-hidden="true"></i>').addClass('alert-danger');

                                        }else{
                                            $('#response').show().html('Ocurrio un error en intentando enviar su solicitud, intente nuevamente. <i class="fa fa-ban" aria-hidden="true"></i>').addClass('alert-danger');

                                        }

                                    }

                                } 
              

    					}, 
    					error: function (jqXHR, exception) {
    					       $("#response").removeClass('alert-info');
                                $('#response').html(' Lo sentimos algo salio mal, intente nuevamente  <i class="fa fa-ban" aria-hidden="true"></i>').addClass('alert-danger');

                      
                            var msg = '';
                    
                            if (jqXHR.status === 0) {
                                msg = 'Error: Sin coneccion.\n Verificar red.';
                            } else if (jqXHR.status == 404) {
                                msg = 'Error: Requested page not found. [404]';
                            } else if (jqXHR.status == 500) {
                                msg = 'Error: Internal Server Error [500].';
                            } else if (exception === 'parsererror') {
                                msg = 'Error: Requested JSON parse failed.';
                            } else if (exception === 'timeout') {
                                msg = 'Error: Time out error.';
                            } else if (exception === 'abort') {
                                msg = 'Error: Ajax request aborted.';
                            } else {
                                msg = 'Uncaught Error.\n' + jqXHR.responseText;
                            }
                            console.log(msg);
                        },
                        complete: function(data) {
                              
                            $('#'+idFormulario).show('slow');

                            setTimeout(function() {

                                $("#response").fadeOut('slow');
                                $('.validacion_error').fadeOut('slow').remove();


                            },3500);

                        }
    				});
    			});
    		});

        }


	});
});
   

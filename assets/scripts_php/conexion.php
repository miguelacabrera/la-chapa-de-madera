<?php 

//Uso de composer para instalacion de librerias y dependencias

require 'composer/vendor/autoload.php';
include 'env.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, 'es_MX.UTF-8');

//Creacion de respuestas TRUE | FALSE como respuesta de este script

function error($mensajeError){

    echo  '{"status": "400", "mensaje": "'.$mensajeError.'"}' ;
    exit();

} 

function errorValidacion($mensajeErrorValidacion){

    echo  '{"status": "401", "mensaje": '.$mensajeErrorValidacion.'}' ;
    exit();

} 
function correcto($mensajeCorrecto){
    echo '{"status": "200", "mensaje": "'.$mensajeCorrecto.'"}';
    exit();


} 
  //Funcion para validar los datos
function ValDato($value){
  	$value = trim($value);        //Eliminar espacios a los lados o en blanco
  	$value = strip_tags($value);//Eliminar etiquetas HTML
  	return $value;
}

//var_dump($_POST); die();
//¿Existen campos dentro de nuestro formulario? y ¿Existe una entrada con nombre 'g-recaptcha-response' = token

if(isset($_POST) && isset($_POST['g-recaptcha-response'])){

    //Variables para establecer conexion con GoogleRecaptcha

    $clave_action_googleRecaptcha = CLAVE_ACTION_GOOGLERECAPTCHA;
    
    $secretKey = CLAVE_SECRET_KEY_GOOGLERECAPTCHA;
    $remoteIp = $_SERVER['REMOTE_ADDR'];
	$token_googleRecaptha = $_POST['g-recaptcha-response'];
	
		
	//Una vez que se almacena el token en una variable se elimina de la variable $_POST.
	unset($_POST['g-recaptcha-response']);
	
	
	//Solicitud para validacion de token y obtener una respuesta JSON(esta puede ser TRUE || FALSE) almacenarla en variable decodificada para su uso como matriz
    $reCaptchaValidationUrl = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$token_googleRecaptha&remoteip=$remoteIp");
    $response_recaptcha = json_decode($reCaptchaValidationUrl, TRUE);



    //Obtenemos campos opcionales de formulario en caso de existir
    if(isset($_POST['campos_opcionales'])){
        
        $campos_opcionales = explode(",", $_POST['campos_opcionales']);
    }else{
        $campos_opcionales = false;
    }
    
    //Una vez que se almacena la existencia de campos opcionales en una variable se elimina de la variable $_POST.
    unset($_POST['campos_opcionales']);
 
        
        
    // 1° y 2º Validacion Recaptcha success TRUE or FALSE , action : $clave_action_googleRecaptcha
    
    if($response_recaptcha["success"] == 1 && $response_recaptcha["action"] == $clave_action_googleRecaptcha ){
        
        // 3° Validacion Recaptcha calificacion mayor o igual a 0.5 
        
    	if($response_recaptcha["score"]>=0.5) {
    	    
    	   $respuesta_calificacion = 'Usuario con calificacion aceptable : '.$response_recaptcha["score"];
    	}else{
    	    
    	   $respuesta_calificacion = 'Usuario con calificacion <strong>baja</strong> favor de revisar el contexto del mensaje : '.$response_recaptcha["score"];
    	}
    	
    	//Creacion de variables para almacenar (Mensaje de respuesta a administrador, mensajes de error y query para almacenar en base de datos)
    	$Mensaje_respuesta_admin  = "<strong style='color:#f75454;'>Nuevo mensaje de formulario con los siguientes datos:</strong> <br>";
    	$msj_validaciones_error = new stdClass();

    	$query_inser = [];
    	
    	
    	
    	//Iteracion de datos enviados desde formulario
    	foreach($_POST as $key => $value){
    	        			    
    	   $value = ValDato($value);
    	    
    	    //Validacion si algun campo vacio es opcional u obligatorio
    	    if($value == '' || $value == NULL || empty($value)){
    	       if (in_array($key, $campos_opcionales)) {
                    $value = 'Usuario no ingreso informacion en este campo opcional';
                    continue;

                    
                }else{
                    //El campo no es opcional, sin embargo se mando vacio

                   $msj_validaciones_error->$key = "El campo ".$key." es obligatorio.";

                }
                
    	    }else{
    	        
    	        //El campo no esta vacio , procede a validacion de respectivo campo
    	        if($key=='nombre'){
    	            
                    if(strlen($value) >= 3 and strlen($value) <= 50){
                        $nombre = $value;
                    }else {
                         $msj_validaciones_error->$key = "El campo ".$key." debe contener entre 3 y 50 caracteres.";
                         
                    }
                    
                    
    			}else if($key=="correo"){
    			    
    				if(filter_var($value, FILTER_VALIDATE_EMAIL)){
    				    
    				    if(strlen($value) >= 3 and strlen($value) <= 50 ){
                           $correo = $value;

                        }else {
                            
                            $msj_validaciones_error->$key = "El campo ".$key." debe contener entre 3 y 50 caracteres.";


                        }
    				}else{
    				    
    				    $msj_validaciones_error->$key = "El correo ingresado no es valido";
    				    


    				    
    				}
                    
    			}else if($key=="telefono"){
    			    
                    if(strlen($value) > 9 && strlen($value) < 11){
                        //
                    }else{
                        
    				   $msj_validaciones_error->$key = "Ingrese un ".$key." a 10 digitos ";


                    }
                    
    			}else if($key=="mensaje"){
    			    
                    
                    if(strlen($value) >= 5 and strlen($value) <= 300){
                        //
                    }else{
                        
                        $msj_validaciones_error->$key =  "El campo ".$key." debe contener entre 5 y 300 caracteres.";

                    }
                    
    			 }
    			 
    			 
    			 else if($key=="referencia"){
    			     $query[$key] = $value;
                     $referencia= $value;
    			     
    			     
    			 }
    			 
    			 else if($key=="formulario"){
    			     $formulario = $value;
                        
    			 }
    	    }
    	    
    	    $Mensaje_respuesta_admin .= '<strong>'.$key.' : </strong>'.$value.'<br>';
            $query[$key] = $value;
    	    
    	}
    	
    	
    	if(!empty($msj_validaciones_error && !empty((array)$msj_validaciones_error))){
    	    
    	   // var_dump($msj_validaciones_error); die();
    	   errorValidacion( json_encode($msj_validaciones_error) );

    	}else{
    	
    	
            //Creacion de varibles de tiempo
            $dateBD=date('Y-m-d H:i:s');
            $date_format = strftime("%A, %d de %B de %Y , %H:%M:%S");
            	
            //Concatenando mensaje de calificacion (score) y fecha de envio.
        	$Mensaje_respuesta_admin .= $respuesta_calificacion.'<br>';
        	$Mensaje_respuesta_admin .= '<strong> Fecha : </strong>'.$date_format.'<br>'; 
    
            
            //Creacion de variables de conexion a Base de datos
            
            
            $servername = DB_HOST;
            $username = DB_USUARIO;
            $password = DB_CONTRA;
            $database = DB_NOMBRE;
        
        
            $sql = "mysql:host=$servername;dbname=$database;";
            $dsn_Options = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
        	
        	//Intenta realizar conexion a base de datos
        	try{ 
                $ConnBD = new PDO($sql, $username, $password, $dsn_Options);
    
            }catch (PDOException $error) {
                 die('Connection error: ' . $error->getMessage());
            }
        
            //Creamos estructura de query para insertar en base de datos 
            $query_insert = 'INSERT INTO contactos (nombre , correo , telefono , respuestas , referencia , actual , ip , fecha_creacion , calificacion_captcha) 
                             VALUES ("'.$query['nombre'].'",
                             "'.$query['correo'].'",
                             "'.$query['telefono'].'",
                             "'.$query['mensaje'].'",
                             "'.$query['referencia'].'",
                             "'.$query['formulario'].'",
                             "'.$remoteIp.'",
                             "'.$dateBD.'",
                             '.$response_recaptcha["score"].');';
    
            //Prepara el query, valida que todo este bien estructurado para su ejecucion
            $prepare_sql = $ConnBD->prepare($query_insert);
        	
        	//Valida la ejecucion de query TRUE || FALSE
        	if(!$prepare_sql->execute()) {
        	    
        	    error("Lo sentimos, ocurrio un error.Por favor intentelo nuevamente"); //Error en ejecutar query en base de datos
            }
    

            $mail = new PHPMailer (true);
    
            try{
                //Configuraciones del servidor para envio de correos
                
                //$mail->SMTPDebug = 2;

                $mail->isSMTP();                                            
                $mail->Host       = MAIL_HOST;  
                $mail->SMTPAuth   = true;   
                $mail->SMTPSecure= false;                                
                $mail->Username   = MAIL_USERNAME;                    
                $mail->Password   = MAIL_PASSWORD;  
                //$mail->AuthType = "LOGIN";                             
                //$mail->SMTPSecure = MAIL_SMTPSecure;                                
                $mail->Port       = MAIL_PORT; 
                $mail->CharSet = MAIL_CharSet; 
                $mail->isHTML(true);
                $mail->setFrom(MAIL_setFrom, MAIL_ADDADDRESS_NAME);

                // Correo para administrador
                $mail->addAddress(MAIL_ADDADDRESS, MAIL_ADDADDRESS_NAME);
                $mail->addBCC('programacion@nubedigital.mx','Elena Sandoval');//$email
                $mail->addBCC('leisygonzalez@gmail.com','Leisy Glz');
                $mail->addBCC('leisy@signe360.com');  
                //  $mail->addBCC('programacion@nubedigital.mx');  

                // Cuerpo y asunto para mensaje administrador
                $mail->Subject = 'Nuevo mensaje del formulario de '.$formulario.' '.MAIL_ADDADDRESS_NAME;
                $mail->Body    = $Mensaje_respuesta_admin;
                $mail->AltBody = $Mensaje_respuesta_admin;
                $mail->send();
                $mail->ClearAddresses();
                
                //Correo Usuario
                $mail->addAddress($correo);
                
                // Cuerpo y asunto para mensaje administrador
                $mail->Subject = MAIL_SUBJECT;
                $mail->Body = MAIL_BODY;
                $mail->AltBody = MAIL_ALTBODY;
                $mail->send();
    
        	    correcto("Su mensaje ha sido enviado de manera correcta"); //Unico caso de funcion correcto
    
            }catch (Exception $e){
                error("Ocurrio un error, porfavor volver a intentarlo mas tarde ".$e);
            } 
        }
    }else{


        error("GoogleRecaptcha ha rechazado la solicitud por un fallo en autentificacion".json_encode($response_recaptcha));
    } 
    
}else{
	error("Se ha enviado un formulario vacio, intentalos nuevamente");
} 









?>
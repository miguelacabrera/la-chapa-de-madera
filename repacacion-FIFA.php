<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reparación FIFA</title>
    
        <?PHP require_once("./scripts_css.php"); ?>
</head>
<body>
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Reparación FIFA</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li class="active">Reparación FIFA</li>
            </ol>
            </div>
        </div>
</section>

<section class="row sectpad services">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="content-1">
                        <div class="row m0 tab_inn_cont_1 ">
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-8">
                                    <div class="row m0 section_header color">
                                        <h2>Reparación FIFA</h2> 
                                    </div>
                        
                                    <p>Engrapadoras, clavadoras y demás elementos marca FIFA.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <img src="/assets/images/servicios/reparacion-FIFA-chapasdemadera.jpg" alt="reparacion-FIFA">
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                     
        </div>
    </div>
</section>

<?PHP require_once("footer.php"); ?>
</body>
</html>
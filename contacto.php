<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <title>Contacto | Chapas de Madera</title>
    <?PHP require_once("./scripts_css.php"); ?>



</head>

<body>
    <?PHP require_once("header.php"); ?>

    <section class="row header-breadcrumb">
        <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Contacto</h2>
                <ol class="breadcrumb">
                    <li><a href="/">Inicio</a></li>
                    <li class="active">Contacto</li>
                </ol>
            </div>
        </div>
    </section>

    <section class="row touch">
        <div class="sectpad touch_bg">
            <div class="container">
                <div class="row touch_middle">
                    <div class="col-md-4 open_hours">
                        <div class="row m0 touch_top">
                            <ul class="nav">
                                <li class="item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="/">
                                                <i class="fa fa-map-marker"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <strong>La ChaPa Querétaro</strong><br />
                                            José Sotelo 1, B. Colonia Aragón C.P. 76040. Santiago de Querétaro
                                        </div>
                                    </div>
                                </li>

                                <li class="item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <i class="fa fa-envelope-o"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            ventalachapa@gmail.com
                                        </div>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <i class="fa fa-phone"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            442-2241-555
                                        </div>
                                    </div>
                                </li>
                                <h3>Otras Sucursales</h3>
                                <li class="item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <i class="fa fa-map-marker"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            <strong>La ChaPa Bosque Rel</strong><br />
                                            Camino a Huixquilucan, Lote 7 Mz 2 bis, Esquina Monte Alegre, Col. El pedregal huixquilucan Edo. de México
                                        </div>
                                    </div>
                                </li>
                                <li class="item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#">
                                                <i class="fa fa-phone"></i>
                                            </a>
                                        </div>
                                        <div class="media-body">
                                            16-62-67-47
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    
                    <div class="col-md-8 input_form">
                       <div id="response" class="alert text-center"></div>
                            <form role="form" action="assets/scripts_php/envio.php" id="contactForm" method="post" >
                                <div class="row">
                                    <div class="col-sm-12 list-stylenone"> 
                                        <label for="nombre">
                                        <input type="text" name="nombre"  id="nombre" placeholder="Ingresa tu nombre completo" required minlength="3" maxlength="50" class="form-control">

                                        </label>
                                    </div>
                                    <div class="col-sm-12 list-stylenone">
                                        <label name="correo">
                                        <input type="email"  name="correo" id="correo" placeholder="Ingresa tu correo electrónico" required class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">

                                        </label>
                                    </div>
                                    <div class="col-sm-12 list-stylenone">
                                        <label for="telefono">
                                        <input type="text" name="telefono" id="telefono" placeholder="Ingresa tu teléfono de contacto"   maxlength="10"   minlength="10" class="form-control">

                                        </label>
                                    </div>
                                    <div class="col-sm-12 list-stylenone">
                                        <label for="mensaje">
                                        <textarea id="mensaje" name="mensaje" placeholder="Ingresa algun mensaje"  class="form-control" minlength= "5" maxlength="300" required rows="9"></textarea>

                                        </label>
                                    </div>
                                    <div class="col-sm-12 list-stylenone">

                                        <input type="hidden" name="referencia" id="referencia" value="<?php echo $_SERVER['REQUEST_URI']; ?>">
                        
                                        <input type="hidden" name="formulario" value="Contacto">
                                        <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response" value="">
                                        <button type="submit"  class="btn btn-default" name="enviar" >Mandar mensaje <i class="fa fa-caret-right"></i></button>

                                    </div>
                                </div>
                            </form>
                    
                        </div>
                   </div>
                </div>
            </div>
        </div>
    </section>

    <section class="map">
        <div id="map" style="height:400px" class="row m0" data-lat="20.578754" data-lon="-100.385732" data-zoom="15"></div>
        <script>
            var map;

            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {
                        lat: 20.578754,
                        lng: -100.385732
                    },
                    zoom: 15
                });

                var marker = new google.maps.Marker({
                    position: {
                        lat: 20.578754,
                        lng: -100.385732
                    },
                    map: map,
                    title: 'Chapas de Madera Exotic',
                    animation: google.maps.Animation.DROP,
                    icon: 'http://www.chapasdemaderaexotica.com.mx/images/marker-lachapa.png'
                });
                marker.addListener('click', toggleBounce);

                function toggleBounce() {
                    if (marker.getAnimation() !== null) {
                        marker.setAnimation(null);
                    } else {
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    }
                }
            }
        </script>
    </section>


    <?PHP require_once("footer.php"); ?>
    
   


</body>

</html>
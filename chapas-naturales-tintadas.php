<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chapas de madera  tintadas</title>
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body class="st">
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Chapas tintadas</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Chapas tintadas</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/eucalipto-azabache-chapa-madera-tintadas-320x530.jpg" data-source="projects-details.html" title="Eucalipto Azabache" data-desc="Eucalipto Azabache" class="popup">
                        <img src="/assets/chapas/tintadas/eucalipto-azabache-chapa-madera-tintadas-320x530.jpg" alt="Eucalipto Azabache">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Eucalipto Azabache</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-azabache-flake-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Roble Azabache Flake" data-desc="Roble Azabache Flake" class="popup">
                        <img src="/assets/chapas/tintadas/roble-azabache-flake-chapa-madera-texturizada-320x530.jpg" alt="Roble Azabache Flake">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Azabache Flake</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-lugano-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Lugano" data-desc="Roble Lugano" class="popup">
                        <img src="/assets/chapas/tintadas/roble-lugano-chapa-madera-tintada-320x530.jpg" alt="Roble Lugano">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Lugano</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-turin-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Turín" data-desc="Roble Turín" class="popup">
                        <img src="/assets/chapas/tintadas/roble-turin-chapa-madera-tintada-320x530.jpg" alt="Roble Turín">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Turín</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-pizarra-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Pizarra" data-desc="Roble Pizarra" class="popup">
                        <img src="/assets/chapas/tintadas/roble-pizarra-chapa-madera-tintada-320x530.jpg" alt="Roble Pizarra">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Pizarra</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-volpi-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Volpi" data-desc="Roble Volpi" class="popup">
                        <img src="/assets/chapas/tintadas/roble-volpi-chapa-madera-tintada-320x530.jpg" alt="Roble Volpi">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Volpi</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-torino-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Torino" data-desc="Roble Torino" class="popup">
                        <img src="/assets/chapas/tintadas/roble-torino-chapa-madera-tintada-320x530.jpg" alt="Roble Torino">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Torino</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-sabbia-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Sabbia" data-desc="Roble Sabbia" class="popup">
                        <img src="/assets/chapas/tintadas/roble-sabbia-chapa-madera-tintada-320x530.jpg" alt="Roble Sabbia">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Sabbia</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/roble-bellagio-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Roble Bellagio" data-desc="Roble Bellagio" class="popup">
                        <img src="/assets/chapas/tintadas/roble-bellagio-chapa-madera-tintada-320x530.jpg" alt="Roble Bellagio">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Bellagio</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/ancient-rovere-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Ancient Rovere" data-desc="Ancient Rovere" class="popup">
                        <img src="/assets/chapas/tintadas/ancient-rovere-chapa-madera-tintada-320x530.jpg" alt="Ancient Rovere">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ancient Rovere</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-gallipoli-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Gallipoli" data-desc="Fresno Gallipoli" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-gallipoli-chapa-madera-tintada-320x530.jpg" alt="Fresno Gallipoli">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Gallipoli</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-avellana-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Avellana" data-desc="Fresno Avellana" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-avellana-chapa-madera-tintada-320x530.jpg" alt="Fresno Avellana">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Avellana</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--2-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-artico-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Ártico" data-desc="Fresno Ártico" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-artico-chapa-madera-tintada-320x530.jpg" alt="Fresno Ártico">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Ártico</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/bruno-trento-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Bruno Trento" data-desc="Bruno Trento" class="popup">
                        <img src="/assets/chapas/tintadas/bruno-trento-chapa-madera-tintada-320x530.jpg" alt="Bruno Trento">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bruno Trento</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/bruno-verona-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Bruno Verona" data-desc="Bruno Verona" class="popup">
                        <img src="/assets/chapas/tintadas/bruno-verona-chapa-madera-tintada-320x530.jpg" alt="Bruno Verona">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bruno Verona</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/gris-modena-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Gris Módena" data-desc="Gris Módena" class="popup">
                        <img src="/assets/chapas/tintadas/gris-modena-chapa-madera-tintada-320x530.jpg" alt="Gris Módena">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Gris Módena</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/gris-brescia-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Gris Brescia" data-desc="Gris Brescia" class="popup">
                        <img src="/assets/chapas/tintadas/gris-brescia-chapa-madera-tintada-320x530.jpg" alt="Gris Brescia">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Gris Brescia</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/nogal-forli-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Nogal Forli" data-desc="Nogal Forli" class="popup">
                        <img src="/assets/chapas/tintadas/nogal-forli-chapa-madera-tintada-320x530.jpg" alt="Nogal Forli">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Forli</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/nogal-milano-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Nogal Milano" data-desc="Nogal Milano" class="popup">
                        <img src="/assets/chapas/tintadas/nogal-milano-chapa-madera-tintada-320x530.jpg" alt="Nogal Milano">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Milano</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/silver-lira-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Silver Lira" data-desc="Silver Lira" class="popup">
                        <img src="/assets/chapas/tintadas/silver-lira-chapa-madera-tintada-320x530.jpg" alt="Silver Lira">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Silver Lira</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/black-lira-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Black Lira" data-desc="Black Lira" class="popup">
                        <img src="/assets/chapas/tintadas/black-lira-chapa-madera-tintada-320x530.jpg" alt="Black Lira">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Black Lira</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/douglas-fir-ceniza-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Douglas Fir Ceniza" data-desc="Douglas Fir Ceniza" class="popup">
                        <img src="/assets/chapas/tintadas/douglas-fir-ceniza-chapa-madera-tintada-320x530.jpg" alt="Douglas Fir Ceniza">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Douglas Fir Ceniza</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-ceniza-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Ceniza" data-desc="Fresno Ceniza" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-ceniza-chapa-madera-tintada-320x530.jpg" alt="Fresno Ceniza">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Ceniza</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/tulipe-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Tulipe" data-desc="Tulipe" class="popup">
                        <img src="/assets/chapas/tintadas/tulipe-chapa-madera-tintada-320x530.jpg" alt="Tulipe">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Tulipe</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--3-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-tramonto-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Tramonto" data-desc="Fresno Tramonto" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-tramonto-chapa-madera-tintada-320x530.jpg" alt="Fresno Tramonto">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Tramonto</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/fresno-ancient-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Fresno Ancient" data-desc="Fresno Ancient" class="popup">
                        <img src="/assets/chapas/tintadas/fresno-ancient-chapa-madera-tintada-320x530.jpg" alt="Fresno Ancient">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Ancient</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/primavera-ceniza-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Primavera Ceniza" data-desc="Primavera Ceniza" class="popup">
                        <img src="/assets/chapas/tintadas/primavera-ceniza-chapa-madera-tintada-320x530.jpg" alt="Primavera Ceniza">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Primavera Ceniza</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/gris-maturo-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Gris Maturo" data-desc="Gris Maturo" class="popup">
                        <img src="/assets/chapas/tintadas/gris-maturo-chapa-madera-tintada-320x530.jpg" alt="Gris Maturo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Gris Maturo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tintadas/verde-maturo-chapa-madera-tintada-320x530.jpg" data-source="projects-details.html" title="Verde Maturo" data-desc="Verde Maturo" class="popup">
                        <img src="/assets/chapas/tintadas/verde-maturo-chapa-madera-tintada-320x530.jpg" alt="Verde Maturo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Verde Maturo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/primavera-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Primavera" data-desc="Primavera ceniza" class="popup">
                        <img src="/assets/chapas/primavera-chapa-madera-natural-320x530.jpg" alt="Primavera ceniza">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Primavera ceniza</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            
            


        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>
</body>
</html>
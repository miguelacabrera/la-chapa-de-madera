<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Galería | Chapas de Madera</title>
        <?PHP require_once("./scripts_css.php"); ?>
</head>
<body class="st">
<?PHP require_once("header.php"); ?>
<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Galería</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Galería</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/1-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Decoración en cocina" data-desc="Decoración en cocina" class="popup">
                        <img src="/assets/images/galeria/1-galeria-chapas-de-madera.jpg" alt="Decoración en cocina">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Decoración en cocina</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/2-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Mueble eucalipto ahumado" data-desc="Mueble eucalipto ahumado" class="popup">
                        <img src="/assets/images/galeria/2-galeria-chapas-de-madera.jpg" alt="Mueble eucalipto ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Mueble eucalipto ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/4-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Verde maturo entintado" data-desc="Verde maturo entintado" class="popup">
                        <img src="/assets/images/galeria/4-galeria-chapas-de-madera.jpg" alt="Verde maturo entintado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Verde maturo entintado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/3-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Puertas sobre diseño" data-desc="Puertas sobre diseño" class="popup">
                        <img src="/assets/images/galeria/3-galeria-chapas-de-madera.jpg" alt="Puertas sobre diseñ">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Puertas sobre diseño</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/6-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Diseño de puertas" data-desc="Diseño de puertas" class="popup">
                        <img src="/assets/images/galeria/6-galeria-chapas-de-madera.jpg" alt="Diseño de puertas">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Diseño de puertas</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/7-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Muebles eucalipto ahumado" data-desc="Muebles eucalipto ahumado" class="popup">
                        <img src="/assets/images/galeria/7-galeria-chapas-de-madera.jpg" alt="Muebles eucalipto ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Muebles eucalipto ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/8-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Chapacintas" data-desc="Chapacintas" class="popup">
                        <img src="/assets/images/galeria/8-galeria-chapas-de-madera.jpg" alt="la chapa de madera">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Chapacintas</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/5-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Diseño de cocina con chapas" data-desc="Diseño de cocina con chapas" class="popup">
                        <img src="/assets/images/galeria/5-galeria-chapas-de-madera.jpg" alt="galeria chapas de madera">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Diseño de cocina con chapas</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
             <div class="col-sm-6 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/10-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Planchón largo de madera" data-desc="Planchón largo de madera" class="popup">
                        <img src="/assets/images/galeria/10-galeria-chapas-de-madera.jpg" alt="galeria chapas">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Planchón largo de madera</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-3 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/images/galeria/9-galeria-chapas-de-madera.jpg" data-source="projects-details.html" title="Chapas de madera y pegamentos" data-desc="Chapas de madera y pegamentos" class="popup">
                        <img src="/assets/images/galeria/9-galeria-chapas-de-madera.jpg" alt="galeria">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Chapas de madera y pegamentos</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>
</body>
</html>
<header class="row header navbar-static-top" id="main_navbar"><meta charset="gb18030">
    <div class="container">
        <div class="row m0 social-info">
            <ul class="social-icon">
                <li><a href="https://www.facebook.com/lachapaqueretaro/" target="_blank"><img src="/facebook-icono-lachapa.png"></a></li>
                <li class="tel"><a href="tel:+1234567890"><i class="fa fa-phone"></i> (442) 2241-555 </a></li>
            </ul>
        </div>
    </div>
   <div class="logo_part">
        <div class="logo">
            <a href="/" class="brand_logo">
                <img src="/assets/images/header/logo.png" alt="logo image">
            </a>
        </div>
    </div>
    <div class="main-menu">
        <nav class="navbar navbar-default">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
     
            
            <div class="menu row m0">                
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a href="/">INICIO</a>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Chapas de madera</a>
                            <ul class="dropdown-menu">
                                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Chapas naturales</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="chapas-naturales-sin-tratamiento">Sin tratamiento</a></li>
                                        <li><a href="chapas-naturales-texturizadas">Texturizadas</a></li>
                                        <li><a href="chapas-naturales-ahumadas">Ahumadas</a></li>
                                        <li><a href="chapas-naturales-tintadas">Tintadas</a></li>
                                    </ul>
                                </li>
                                <li><a href="chapas-precompuestas">Chapas precompuestas</a>
                                </li>
                            </ul>
                        </li>
                         <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Productos</a>
                            <ul class="dropdown-menu">
                                <li><a href="enchapados">Enchapados</a></li>
                                <li><a href="chapacinta">Chapacinta</a></li>
                                <li><a href="formaicas">Formaicas</a></li>
                                <li><a href="planchones-y-rodajas">Planchones y rodajes</a></li>
                                <li><a href="raices-y-rarezas">Raíces y Rarezas</a></li>
                                <li><a href="aceite-y-vida-madera">Aceite y vida madera</a></li>
                                <li><a href="engrapadora-y-clavadora">Engrapadora y clavadora</a></li>
                                <li><a href="grapas-y-clavillos">Grapas y clavillos</a></li>
                                <li><a href="corcho">Corcho</a></li>
                                <li><a href="pegamentos">Pegamentos</a></li>
                                <li><a href="#">Herramienta de carpinteria</a></li>
                                <li><a href="playo-hule-burbuja-polifon">Playo, hule burbuja y polifón</a></li>
                                <li><a href="aluminio">Aluminio</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Servicios</a>
                            <ul class="dropdown-menu">
                                <li><a href="repacacion-FIFA">Reparación FIFA</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="galeria">Galería</a>
                        </li>
                        <li><a href="contacto">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
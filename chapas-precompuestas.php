<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chapas de madera precompuestas</title>
    <?PHP require_once("./scripts_css.php"); ?>

</head>
<body class="st">
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Chapas precompuestas</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Chapas precompuestas</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-kasai-chapa-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Kasai" data-desc="Wengue Kasai" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-kasai-chapa-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Kasai</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-komo-chapa-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Komo" data-desc="Wengue Komo" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-komo-chapa-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Komo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-lendu-chapa-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Lendu" data-desc="Wengue Lendu" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-lendu-chapa-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Lendu</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-malawi-chapa-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Malawi" data-desc="Wengue Malawi" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-malawi-chapa-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Malawi</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-multi-chapa-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Multi" data-desc="Wengue Multi" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-multi-chapa-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Multi</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-niger-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Niger" data-desc="Wengue Niger" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-niger-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Niger</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-luba-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Luba" data-desc="Wengue Luba" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-luba-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Luba</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-nuovo-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Nouvo" data-desc="Wengue Nouvo" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-nuovo-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Nouvo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-oreo-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Oreo" data-desc="Wengue Oreo" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-oreo-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Oreo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-veri-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Veri" data-desc="Wengue Veri" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-veri-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Veri</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-zaire-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Zaire" data-desc="Wengue Zaire" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-zaire-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Zaire</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/precompuestas/wengue-sadik-madera-precompuesta-320x530.jpg" data-source="projects-details.html" title="Wengue Sadik" data-desc="Wengue Sadik" class="popup">
                        <img src="/assets/chapas/precompuestas/wengue-sadik-madera-precompuesta-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengue Sadik</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            
            
            


        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>
</body>
</html>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chapas de madera sin tratamiento</title>
    <?PHP require_once("./scripts_css.php"); ?>

</head>
<body class="st">
<?PHP require_once("header.php"); ?>

<!--breadcrumb-->
<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Chapas sin tratamiento</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Chapas naturales</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ostra-de-nogal-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Ostra de Nogal" data-desc="Ostra de Nogal" class="popup">
                        <img src="/assets/chapas/ostra-de-nogal-chapa-madera-natural-320x530.jpg" alt="Ostra de Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ostra de Nogal</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-de-mapa-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Mapa" data-desc="Raíz de Mapa" class="popup">
                        <img src="/assets/chapas/raiz-de-mapa-chapa-madera-natural-320x530.jpg" alt="Raíz de Mapa">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Mapa</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-fresno-olivato-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Fresno Olivato" data-desc="Raíz de fresno Olivato" class="popup">
                        <img src="/assets/chapas/raiz-fresno-olivato-chapa-madera-natural-320x530.jpg" alt="Raíz de Fresno Olivato">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Fresno Olivato</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-nogal-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Nogal" data-desc="Raíz de Nogal" class="popup">
                        <img src="/assets/chapas/raiz-nogal-chapa-madera-natural-320x530.jpg" alt="Raíz de Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Nogal</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-de-madrono-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Madroño" data-desc="Raíz de Madroño" class="popup">
                        <img src="/assets/chapas/raiz-de-madrono-chapa-madera-natural-320x530.jpg" alt="aíz de Madroño">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Madroño</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-de-olmo-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Olmo" data-desc="Raíz de Olmo" class="popup">
                        <img src="/assets/chapas/raiz-de-olmo-chapa-madera-natural-320x530.jpg" alt="Raíz de Olmo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Olmo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/raiz-de-bubinga-pomele-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Raíz de Bubinga Pomelé" data-desc="Raíz de Bubinga Pomelé" class="popup">
                        <img src="/assets/chapas/raiz-de-bubinga-pomele-chapa-madera-natural-320x530.jpg" alt="Raíz de Bubinga Pomelé">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Raíz de Bubinga Pomelé</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/sycomoro-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Sycomoro" data-desc="Sycomoro" class="popup">
                        <img src="/assets/chapas/sycomoro-chapa-madera-natural-320x530.jpg" alt="Sycomoro">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sycomoro</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/sycomoro-friseado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Sycomoro Friseado" data-desc="Sycomoro Friseado" class="popup">
                        <img src="/assets/chapas/sycomoro-friseado-chapa-madera-natural-320x530.jpg" alt="Sycomoro Friseado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sycomoro Friseado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/anigree-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Anigree" data-desc="Anigree" class="popup">
                        <img src="/assets/chapas/anigree-chapa-madera-natural-320x530.jpg" alt="Anigree">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Anigree</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/anigree-friseado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Anigree Friseado" data-desc="Anigree Friseado" class="popup">
                        <img src="/assets/chapas/anigree-friseado-chapa-madera-natural-320x530.jpg" alt="Anigree Friseado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Anigree Friseado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/alder-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Alder" data-desc="Alder" class="popup">
                        <img src="/assets/chapas/alder-chapa-madera-natural-320x530.jpg" alt="Alder">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Alder</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--2-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/alerce-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Alerce Natural" data-desc="Alerce Natural" class="popup">
                        <img src="/assets/chapas/alerce-natural-chapa-madera-natural-320x530.jpg" alt="Alerce Natural">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Alerce Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/peral-suizo-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Peral Suizo" data-desc="Peral Suizo" class="popup">
                        <img src="/assets/chapas/peral-suizo-chapa-madera-natural-320x530.jpg" alt="Peral Suizo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Peral Suizo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/haya-vaporizada-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Haya Vaposizada" data-desc="Haya Vaposizada" class="popup">
                        <img src="/assets/chapas/haya-vaporizada-chapa-madera-natural-320x530.jpg" alt="Haya Vaposizada">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Haya<br >Vaporizada</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/bambu-natural-flat-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Bambú Natural Flat" data-desc="Bambú Natural Flat" class="popup">
                        <img src="/assets/chapas/bambu-natural-flat-chapa-madera-natural-320x530.jpg" alt="Bambú Natural Flat">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bambú Natural Flat</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/bambu-caramelo-flat-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Bambú Caramelo Flat" data-desc="Bambú Caramelo Flat" class="popup">
                        <img src="/assets/chapas/bambu-caramelo-flat-chapa-madera-natural-320x530.jpg" alt="Bambú Natural Flat">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bambú Caramelo Flat</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/bambu-natural-quarter-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Bambú Natural Quarter" data-desc="Bambú Natural Quarter" class="popup">
                        <img src="/assets/chapas/bambu-natural-quarter-chapa-madera-natural-320x530.jpg" alt="Bambú Natural Quarter">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bambú Natural Quarter</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/bambu-caramelo-quarter-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Bambú Caramelo Quarter" data-desc="Bambú Caramelo Quarter" class="popup">
                        <img src="/assets/chapas/bambu-caramelo-quarter-chapa-madera-natural-320x530.jpg" alt="Bambú Natural Quarter">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Bambú Caramelo Quarter</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/zembrano-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Zembrano Natural" data-desc="Zembrano Natural" class="popup">
                        <img src="/assets/chapas/zembrano-natural-chapa-madera-natural-320x530.jpg" alt="Zembrano Natural">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Zembrano Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/teka-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Teka Natural" data-desc="Teka Natural" class="popup">
                        <img src="/assets/chapas/teka-natural-chapa-madera-natural-320x530.jpg" alt="Teka Natural">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Teka Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/teka-africana-rayada-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Teka Africana Rayada" data-desc="Teka Africana Rayada" class="popup">
                        <img src="/assets/chapas/teka-africana-rayada-chapa-madera-natural-320x530.jpg" alt="Teka Africana Rayada">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Teka Africana Rayada</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/teka-africana-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Teka Africana" data-desc="Teka Africana" class="popup">
                        <img src="/assets/chapas/teka-africana-chapa-madera-natural-320x530.jpg" alt="Teka Africana">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Teka Africana</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/sapelly-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Sapelly Rayado" data-desc="Sapelly Rayado" class="popup">
                        <img src="/assets/chapas/sapelly-rayado-chapa-madera-natural-320x530.jpg" alt="Sapelly Rayado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sapelly Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--3-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/sapelly-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Sapelly Flama" data-desc="Sapelly Flama" class="popup">
                        <img src="/assets/chapas/sapelly-flama-chapa-madera-natural-320x530.jpg" alt="Sapelly Flama">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sapelly Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/sapelly-pomelle-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Sapelly Pomellé" data-desc="Sapelly Pomellé" class="popup">
                        <img src="/assets/chapas/sapelly-pomelle-chapa-madera-natural-320x530.jpg" alt="Sapelly Pomellé">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sapelly Pomellé</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/guanacastle-parota-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Guanacastle Parota" data-desc="Guanacastle Parota" class="popup">
                        <img src="/assets/chapas/guanacastle-parota-chapa-madera-natural-320x530.jpg" alt="Guanacastle Parota">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Guanacastle Parota</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/palo-de-rosa-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Palo de Rosa Rayado" data-desc="Palo de Rosa Rayado" class="popup">
                        <img src="/assets/chapas/palo-de-rosa-rayado-chapa-madera-natural-320x530.jpg" alt="Palo de Rosa Rayado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Palo de Rosa Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/jatoba-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Jatoba" data-desc="Jatoba" class="popup">
                        <img src="/assets/chapas/jatoba-chapa-madera-natural-320x530.jpg" alt="Jatoba">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Jatoba</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ebano-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Ébano Natural" data-desc="Ébano Natural" class="popup">
                        <img src="/assets/chapas/ebano-natural-chapa-madera-natural-320x530.jpg" alt="Ébano Natural">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ébano Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tzalam-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Tzalam" data-desc="Tzalam" class="popup">
                        <img src="/assets/chapas/tzalam-chapa-madera-natural-320x530.jpg" alt="Tzalam">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Tzalam</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tzalam-plus-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Tzalam Plus" data-desc="Tzalam Plus" class="popup">
                        <img src="/assets/chapas/tzalam-plus-chapa-madera-natural-320x530.jpg" alt="Tzalam Plus">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Tzalam Plus</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/wengue-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Wengué Flama" data-desc="Wengué Flama" class="popup">
                        <img src="/assets/chapas/wengue-flama-chapa-madera-natural-320x530.jpg" alt="Wengué Flama">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengué Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/wengue-rayado-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Wengué Rayado Natura" data-desc="Wengué Rayado Natura" class="popup">
                        <img src="/assets/chapas/wengue-rayado-natural-chapa-madera-natural-320x530.jpg" alt="Wengué Rayado Natura">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Wengué Rayado Natura</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ciricote-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Ciricote" data-desc="Ciricote" class="popup">
                        <img src="/assets/chapas/ciricote-natural-chapa-madera-natural-320x530.jpg" alt="Ciricote">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ciricote</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--4-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/louro-prieto-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Louro Preto" data-desc="Louro Preto" class="popup">
                        <img src="/assets/chapas/louro-prieto-chapa-madera-natural-320x530.jpg" alt="Louro Preto">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Louro Preto</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ipe-diseno-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="IPE Diseño" data-desc="IPE Diseño" class="popup">
                        <img src="/assets/chapas/ipe-diseno-chapa-madera-natural-320x530.jpg" alt="IPE Diseño">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>IPE Diseño</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ipe-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="IPE" data-desc="IPE" class="popup">
                        <img src="/assets/chapas/ipe-chapa-madera-natural-320x530.jpg" alt="IPE">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>IPE</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/paldao-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Paldao Rayado" data-desc="Paldao Rayado" class="popup">
                        <img src="/assets/chapas/paldao-rayado-chapa-madera-natural-320x530.jpg" alt="Paldao Rayado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Paldao Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/pladao-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Paldao Flama" data-desc="Paldao Flama" class="popup">
                        <img src="/assets/chapas/pladao-flama-chapa-madera-natural-320x530.jpg" alt="Paldao Flama">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Paldao Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/limba-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Limba" data-desc="Limba" class="popup">
                        <img src="/assets/chapas/limba-chapa-madera-natural-320x530.jpg" alt="Limba">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Limba</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/knotty-pine-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Knotty Pine" data-desc="Knotty Pine" class="popup">
                        <img src="/assets/chapas/knotty-pine-chapa-madera-natural-320x530.jpg" alt="">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Knotty Pine</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/poplar-blanco-USA-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Poplar Blanco USA" data-desc="Poplar Blanco USA" class="popup">
                        <img src="/assets/chapas/poplar-blanco-USA-chapa-madera-natural-320x530.jpg" alt="Knotty Pine">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Poplar Blanco USA</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/poplar-USA-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Poplar USA" data-desc="Poplar USA" class="popup">
                        <img src="/assets/chapas/poplar-USA-chapa-madera-natural-320x530.jpg" alt="Poplar USA">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Poplar USA</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/pino-USA-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Pino USA Rayado" data-desc="Pino USA Rayado" class="popup">
                        <img src="/assets/chapas/pino-USA-rayado-chapa-madera-natural-320x530.jpg" alt="Pino USA">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Pino USA Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/pino-USA-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Pino USA Flama" data-desc="Pino USA Flama" class="popup">
                        <img src="/assets/chapas/pino-USA-flama-chapa-madera-natural-320x530.jpg" alt="Pino">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Pino USA Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/maple-usa-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Maple USA" data-desc="Maple USA" class="popup">
                        <img src="/assets/chapas/maple-usa-chapa-madera-natural-320x530.jpg" alt="Maple">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Maple USA</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--5-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/parsimonio-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Parsimonio" data-desc="Parsimonio" class="popup">
                        <img src="/assets/chapas/parsimonio-chapa-madera-natural-320x530.jpg" alt="Parsimonio">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Parsimonio</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/tilo-salvaje-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Tilo Salvaje" data-desc="Tilo Salvaje" class="popup">
                        <img src="/assets/chapas/tilo-salvaje-chapa-madera-natural-320x530.jpg" alt="Tilo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Tilo Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/encino-blanco-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Encino Blanco" data-desc="Encino Blanco Rayado" class="popup">
                        <img src="/assets/chapas/encino-blanco-rayado-chapa-madera-natural-320x530.jpg" alt="Encino Blanco">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Encino Blanco Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/encino-blanco-salvaje-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Encino Blanco Salvaje" data-desc="Encino Blanco Salvaje" class="popup">
                        <img src="/assets/chapas/encino-blanco-salvaje-chapa-madera-natural-320x530.jpg" alt="Encino Blanco Salvaje">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Encino Blanco Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/encino-blanco-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Encino Blanco Flama" data-desc="Encino Blanco Flama" class="popup">
                        <img src="/assets/chapas/encino-blanco-flama-chapa-madera-natural-320x530.jpg" alt="Encino Blanco Flama">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Encino Blanco Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/encino-rojo-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Encino Rojo Rayado" data-desc="Encino Rojo Rayado" class="popup">
                        <img src="/assets/chapas/encino-rojo-rayado-chapa-madera-natural-320x530.jpg" alt="Encino">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Encino Rojo Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/encino-rojo-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Encino Rojo Flama" data-desc="Encino Rojo Flama" class="popup">
                        <img src="/assets/chapas/encino-rojo-flama-chapa-madera-natural-320x530.jpg" alt="Encino">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Encino Rojo Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/lancewood-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Lacewood" data-desc="Lacewood" class="popup">
                        <img src="/assets/chapas/lancewood-chapa-madera-natural-320x530.jpg" alt="Lacewood">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Lacewood</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/cedro-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Cedro" data-desc="Cedro" class="popup">
                        <img src="/assets/chapas/cedro-chapa-madera-natural-320x530.jpg" alt="Cedro">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Cedro</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/etimoe-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Etimoe" data-desc="Etimoe" class="popup">
                        <img src="/assets/chapas/etimoe-chapa-madera-natural-320x530.jpg" alt="Etimoe">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Etimoe</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/nogal-frances-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Nogal Francés" data-desc="Nogal Francés" class="popup">
                        <img src="/assets/chapas/nogal-frances-chapa-madera-natural-320x530.jpg" alt="Francés">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Francés</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/nogal-australiano-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Nogal Australiano" data-desc="Nogal Australiano" class="popup">
                        <img src="/assets/chapas/nogal-australiano-chapa-madera-natural-320x530.jpg" alt="Australiano">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Australiano</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--6-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/nogal-americano-salvaje-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Nogal Americano Salvaje" data-desc="Nogal Americano Salvaje" class="popup">
                        <img src="/assets/chapas/nogal-americano-salvaje-chapa-madera-natural-320x530.jpg" alt="Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Americano Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/nogal-americano-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Nogal Americano Rayado" data-desc="Nogal Americano Rayado" class="popup">
                        <img src="/assets/chapas/nogal-americano-rayado-chapa-madera-natural-320x530.jpg" alt="Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Americano Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/nogal-americano-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Nogal americano Flama" data-desc="Nogal americano Flama" class="popup">
                        <img src="/assets/chapas/nogal-americano-flama-chapa-madera-natural-320x530.jpg" alt="Nogal">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal americano Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/fresno-rayado-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Fresno Rayado" data-desc="Fresno Rayado" class="popup">
                        <img src="/assets/chapas/fresno-rayado-chapa-madera-natural-320x530.jpg" alt="Fresno">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Rayado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/fresno-olivato-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Fresno Olivato" data-desc="Fresno Olivato" class="popup">
                        <img src="/assets/chapas/fresno-olivato-chapa-madera-natural-320x530.jpg" alt="Fresno">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Olivato</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/fresno-negro-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Fresno negro" data-desc="Fresno negro" class="popup">
                        <img src="/assets/chapas/fresno-negro-chapa-madera-natural-320x530.jpg" alt="Fresno">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno negro</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/fresno-flama-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Fresno Flama" data-desc="Fresno Flama" class="popup">
                        <img src="/assets/chapas/fresno-flama-chapa-madera-natural-320x530.jpg" alt="Fresno">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Flama</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/padouk-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Padouk Natural" data-desc="Padouk Natural" class="popup">
                        <img src="/assets/chapas/padouk-natural-chapa-madera-natural-320x530.jpg" alt="Padouk">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Padouk Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/manzano-india-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Manzano de India" data-desc="Manzano de India" class="popup">
                        <img src="/assets/chapas/manzano-india-chapa-madera-natural-320x530.jpg" alt="Manzano">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Manzano de India</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/caoba-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Caoba" data-desc="Caoba" class="popup">
                        <img src="/assets/chapas/caoba-chapa-madera-natural-320x530.jpg" alt="Caoba">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Caoba</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/liquid-ambar-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Liquid Ambar" data-desc="Liquid Ambar" class="popup">
                        <img src="/assets/chapas/liquid-ambar-chapa-madera-natural-320x530.jpg" alt="Liquid">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Liquid Ambar</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/cerezo-usa-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Cerezo USA" data-desc="Cerezo USA" class="popup">
                        <img src="/assets/chapas/cerezo-usa-chapa-madera-natural-320x530.jpg" alt="Cerezo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Cerezo USA</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            <!--7-->
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/roble-europeo-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Roble Europeo Natural" data-desc="Roble Europeo Natural" class="popup">
                        <img src="/assets/chapas/roble-europeo-natural-chapa-madera-natural-320x530.jpg" alt="Roble">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Europeo Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/castano-europeo-natural-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Castaño Europeo" data-desc="Castaño Europeo" class="popup">
                        <img src="/assets/chapas/castano-europeo-natural-chapa-madera-natural-320x530.jpg" alt="Castaño">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Castaño Europeo/p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/koa-hawaii-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Koa Hawaii" data-desc="Koa Hawaii" class="popup">
                        <img src="/assets/chapas/koa-hawaii-chapa-madera-natural-320x530.jpg" alt="Hawaii">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Koa Hawaii</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/cipress-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Cipress" data-desc="Cipress" class="popup">
                        <img src="/assets/chapas/cipress-chapa-madera-natural-320x530.jpg" alt="Cipress">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Cipress</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/granadillo-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Granadillo" data-desc="Granadillo" class="popup">
                        <img src="/assets/chapas/granadillo-chapa-madera-natural-320x530.jpg" alt="Granadillo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Granadillo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ayaous-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Ayaous" data-desc="Ayaous" class="popup">
                        <img src="/assets/chapas/ayaous-chapa-madera-natural-320x530.jpg" alt="Ayaous">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ayaous</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>


            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ahumadas/roble-europeo-salvaje-madera-ahumada-320x530.jpg" data-source="projects-details.html" title="Roble Europeo Salvaje" data-desc="Roble Europeo Salvaje" class="popup">
                        <img src="/assets/chapas/ahumadas/roble-europeo-salvaje-madera-ahumada-320x530.jpg" alt="Salvaje">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble Europeo Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            

            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/primavera-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Primavera" data-desc="Primavera" class="popup">
                        <img src="/assets/chapas/primavera-chapa-madera-natural-320x530.jpg" alt="Primavera">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Primavera</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            

            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/ojo-de-pajaro-chapa-madera-natural-320x530.jpg" data-source="projects-details.html" title="Ojo de pájaro" data-desc="Ojo de pájaro" class="popup">
                        <img src="/assets/chapas/ojo-de-pajaro-chapa-madera-natural-320x530.jpg" alt="Ojo de pájaro">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Ojo de pájaro</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

        </div>
    </div>
</section>

<!--footer--><?PHP require_once("footer.php"); ?>
</body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aceites y vida madera</title>
    
    <?PHP require_once("./scripts_css.php"); ?>
</head>
<body>
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Aceites y vida madera</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li class="active">Aceites y vida madera</li>
            </ol>
            </div>
        </div>
</section>

<section class="row sectpad services">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 sidebar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="enchapados">
                            Enchapados
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="chapacinta">
                           Chapacinta
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="formaicas">
                            Formaicas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="planchones-y-rodajas">
                            Planchones y rodajas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="raices-y-rarezas">
                            Raíces y Rarezas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aceite-y-vida-madera">
                            Aceite y vida madera
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="engrapadora-y-clavadora">
                            Engrapadora y clavadora
                       </a>
                    </li>
                    <li role="presentation">
                        <a href="grapas-y-clavillos">
                            Grapas y clavillos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="corcho">
                            Corcho
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="pegamentos">
                            Pegamentos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="herramienta">
                            Herramienta de carpintería
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="playo-hule-burbuja-polifon">
                            Playo, hule burbuja y polifón
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aluminio">
                            Aluminio
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="content-1">
                        <div class="row m0 tab_inn_cont_1 ">
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-12">
                                    <div class="row m0 section_header color">
                                        <h2>Aceites y Vida Madera</h2> 
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-6"><img src="/assets/images/productos/aceites/3029-osmo-chapasdemadera-1.jpg"></div>
                                        <div class="col-md-6"><a data-fancybox href="/assets/images/productos/aceites/3029-osmo-chapasdemadera-3.jpg"><img src="/assets/images/productos/aceites/3029-osmo-chapasdemadera-2.jpg"></a></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6"><img src="/assets/images/productos/aceites/3065-osmo-chapasdemadera-1.jpg"></div>
                                        <div class="col-md-6"><a data-fancybox href="/assets/images/productos/aceites/3065-osmo-chapasdemadera-3.jpg"><img src="/assets/images/productos/aceites/3065-osmo-chapasdemadera-2.jpg"></a></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6"><img src="/assets/images/productos/aceites/3101-osmo-chapasdemadera-1.jpg"></div>
                                        <div class="col-md-6"><a data-fancybox href="/assets/images/productos/aceites/3101-osmo-chapasdemadera-3.jpg"><img src="/assets/images/productos/aceites/3101-osmo-chapasdemadera-2.jpg"></a></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6"><img src="/assets/images/productos/aceites/3032-osmo-chapasdemadera-1.jpg"></div>
                                        <div class="col-md-6"><a data-fancybox href="/assets/images/productos/aceites/3032-osmo-chapasdemadera-3.jpg"><img src="/assets/images/productos/aceites/3032-osmo-chapasdemadera-2.jpg"></a></div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6"><img src="/assets/images/productos/aceites/jabon-madera-osmo-chapasdemadera-1.jpg"></div>
                                        <div class="col-md-6"><a data-fancybox href="/assets/images/productos/aceites/jabon-madera-osmo-chapasdemadera-3.jpg"><img src="/assets/images/productos/aceites/jabon-madera-osmo-chapasdemadera-2.jpg"></a></div>
                                    </div>
                                    
                                    <h3>Vida Madera</h3>
                                    <p>Se utiliza para preservar la madera y eliminar polillas, termitas, podredumbres, hinchazón, rajaduras y torcimientos.</p>
                                    <p>Disponemos en lts, galón y porrón.</p>
                                    <div class="col-md-6"><img src="/assets/images/productos/aceites/vidamadera-osmo-chapasdemadera-1.jpg" alt=""></div>
                                    <div class="col-md-6"><img src="/assets/images/productos/aceites/vidamadera-osmo-chapasdemadera-2.jpg" alt=""></div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                     
        </div>
    </div>
</section>

<?PHP require_once("footer.php"); ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>
<script type="text/javascript">
    $("[data-fancybox]").fancybox({
       
    });
</script>
</body>
</html>
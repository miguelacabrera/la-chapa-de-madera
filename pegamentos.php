<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Pegamentos</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body>
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Pegamentos</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li class="active">Pegamentos</li>
            </ol>
            </div>
        </div>
</section>

<section class="row sectpad services">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 sidebar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="enchapados">
                            Enchapados
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="chapacinta">
                           Chapacinta
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="formaicas">
                            Formaicas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="planchones-y-rodajas">
                            Planchones y rodajas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="raices-y-rarezas">
                            Raíces y Rarezas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aceite-y-vida-madera">
                            Aceite y vida madera
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="engrapadora-y-clavadora">
                            Engrapadora y clavadora
                       </a>
                    </li>
                    <li role="presentation">
                        <a href="grapas-y-clavillos">
                            Grapas y clavillos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="corcho">
                            Corcho
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="pegamentos">
                            Pegamentos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="herramienta">
                            Herramienta de carpintería
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="playo-hule-burbuja-polifon">
                            Playo, hule burbuja y polifón
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aluminio">
                            Aluminio
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="content-1">
                        <div class="row m0 tab_inn_cont_1 ">
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-8">
                                    <div class="row m0 section_header color">
                                        <h2>Pegamentos</h2> 
                                    </div>
                                    <p>
                                        <ul class="two-col-list nav">
                                            <li>Pegamentos de contacto ( Lokweld 500 ) 1 Lt, 4 Lts, 20 lts, tambor.</li>
                                            <li>Pegamentos de contacto (Resistol 5000) 1 lt, 4 Lts, 20 Lts.</li>
                                            <li>Pegamentos de contacto (Furia) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>Pegamentos de contacto (Pegamex) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>Pegamentos Blanco (Lokweld 40) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>Pegamentos Blanco (RESISTOL 850 ) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>Pegamentos Blanco (FABRICA) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>Pegamentos H20  Galon, Cubeta.</li>
                                            <li>LOKWELD 30 (AMARILLO) 1 lt, 4 Lts, 20 lts.</li>
                                            <li>BLANCO uso general 300 ml</li>
                                            <li>TRANSPARENTE uso general</li>
                                            <li>NEGRO uso general 300 ml</li>
                                            <li>NO MÁS CLAVOS</li>
                                        </ul>
                                    </p>
                                </div>
                                <div class="cont_right col-sm-4">
                                    <img src="/assets/images/productos/pegamentos/pegamento-chapasdemadera1.jpg" alt="pegamento">                                
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                     
        </div>
    </div>
</section>

<?PHP require_once("footer.php"); ?>

</body>
</html>
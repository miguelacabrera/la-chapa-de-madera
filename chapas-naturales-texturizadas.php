<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chapas de madera texturizadas</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body class="st">
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
        <div class="row m0 page-cover">
            <h2 class="page-cover-tittle">Chapas texturizadas</h2>
        <ol class="breadcrumb">
            <li><a href="/">Inicio</a></li>
            <li class="active">Chapas texturizadas</li>
        </ol>
        </div>
    </div>
</section>
<section class="row latest_projects sectpad projects-1">
    <div class="container">
        <div class="projects2 popup-gallery" id="projects">
            <div class="grid-sizer"></div>
            
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/alerce-suizo-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Alerce Suizo" data-desc="Alerce Suizo" class="popup">
                        <img src="/assets/chapas/texturizadas/alerce-suizo-chapa-madera-texturizada-320x530.jpg" alt="Alerce Suizo">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Alerce Suizo</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/alerce-suizo-ahumado-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Alerce Suizo Ahumado" data-desc="Alerce Suizo Ahumado" class="popup">
                        <img src="/assets/chapas/texturizadas/alerce-suizo-ahumado-chapa-madera-texturizada-320x530.jpg" alt="Alerce Suizo Ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Alerce Suizo Ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/roble-e-natural-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Roble E. Natural" data-desc="Roble E. Natural" class="popup">
                        <img src="/assets/chapas/texturizadas/roble-e-natural-chapa-madera-texturizada-320x530.jpg" alt="Roble E. Natural">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble E. Natural</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/roble-e-natural-salvaje-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Roble E. Natural Salvaje" data-desc="Roble E. Natural Salvaje" class="popup">
                        <img src="/assets/chapas/texturizadas/roble-e-natural-salvaje-chapa-madera-texturizada-320x530.jpg" alt="Roble E. Ahumado Salvaje">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble E. Natural Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/roble-e-ahumado-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Roble E. Ahumado" data-desc="Roble E. Ahumado" class="popup">
                        <img src="/assets/chapas/texturizadas/roble-e-ahumado-chapa-madera-texturizada-320x530.jpg" alt="Roble E. Ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble E. Ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/roble-e-ahumado-salvaje-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Roble E. Ahumado Salvaje" data-desc="Roble E. Ahumado Salvaje" class="popup">
                        <img src="/assets/chapas/texturizadas/roble-e-ahumado-salvaje-chapa-madera-texturizada-320x530.jpg" alt="Roble E. Ahumado Salvaje">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Roble E. Ahumado Salvaje</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/nogal-americano-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Nogal Americano" data-desc="Nogal Americano" class="popup">
                        <img src="/assets/chapas/texturizadas/nogal-americano-chapa-madera-texturizada-320x530.jpg" alt="Nogal Americano">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Americano</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/nogal-satin-ahumado-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Nogal Satín Ahumado" data-desc="Nogal Satín Ahumado" class="popup">
                        <img src="/assets/chapas/texturizadas/nogal-satin-ahumado-chapa-madera-texturizada-320x530.jpg" alt="Nogal Satín Ahumad">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Nogal Satín Ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/sapelly-ahumado-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Sapelly Ahumado" data-desc="Sapelly Ahumado" class="popup">
                        <img src="/assets/chapas/texturizadas/sapelly-ahumado-chapa-madera-texturizada-320x530.jpg" alt="Sapelly Ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Sapelly Ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/eucalipto-ahumado-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Eucalipto Ahumado" data-desc="Eucalipto Ahumado" class="popup">
                        <img src="/assets/chapas/texturizadas/eucalipto-ahumado-chapa-madera-texturizada-320x530.jpg" alt="Eucalipto Ahumado">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Eucalipto Ahumado</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/fresno-marron-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Fresno Marrón" data-desc="Fresno Marrón" class="popup">
                        <img src="/assets/chapas/texturizadas/fresno-marron-chapa-madera-texturizada-320x530.jpg" alt="Fresno Marrón">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Marrón</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-1 col-xs-6 project indoor hardwood renovation renovation">
               <div class="project-img">
                    <a href="/assets/chapas/texturizadas/fresno-ceniza-chapa-madera-texturizada-320x530.jpg" data-source="projects-details.html" title="Fresno Ceniza" data-desc="Fresno Ceniza" class="popup">
                        <img src="/assets/chapas/texturizadas/fresno-ceniza-chapa-madera-texturizada-320x530.jpg" alt="Fresno Ceniza">
                    <div class="project-text">
                        <ul class="list-unstyled">
                            <li><i class="icon icon-Search"></i></li>
                        </ul>
                        <div class="row m0">
                            <p>Fresno Ceniza</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
            
            


        </div>
    </div>
</section>
<?PHP require_once("footer.php"); ?>
</body>
</html>
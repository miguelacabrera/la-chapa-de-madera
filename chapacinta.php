<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Chapacinta</title>
    
        <?PHP require_once("./scripts_css.php"); ?>

</head>
<body>
<?PHP require_once("header.php"); ?>

<section class="row header-breadcrumb">
    <div class="container">
            <div class="row m0 page-cover">
                <h2 class="page-cover-tittle">Chapacinta</h2>
            <ol class="breadcrumb">
                <li><a href="/">Inicio</a></li>
                <li class="active">Chapacinta</li>
            </ol>
            </div>
        </div>
</section>

<section class="row sectpad services">
    <div class="container">
        <div class="row">
                        <div class="col-sm-3 sidebar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="enchapados">
                            Enchapados
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="chapacinta">
                           Chapacinta
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="formaicas">
                            Formaicas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="planchones-y-rodajas">
                            Planchones y rodajas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="raices-y-rarezas">
                            Raíces y Rarezas
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aceite-y-vida-madera">
                            Aceite y vida madera
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="engrapadora-y-clavadora">
                            Engrapadora y clavadora
                       </a>
                    </li>
                    <li role="presentation">
                        <a href="grapas-y-clavillos">
                            Grapas y clavillos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="corcho">
                            Corcho
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="pegamentos">
                            Pegamentos
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="herramienta">
                            Herramienta de carpintería
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="playo-hule-burbuja-polifon">
                            Playo, hule burbuja y polifón
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="aluminio">
                            Aluminio
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-9 tab_pages">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="content-1">
                        <div class="row m0 tab_inn_cont_1 ">
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-8">
                                    <div class="row m0 section_header color">
                                        <h2>Chapacinta</h2> 
                                    </div>
                                    <p>Contamos con una variedad de diseños desde 16mm a 100 mm con adhesivos para mejorar acabados en los tableros.</p>
                                    <p>Chapacintas de madera natural:</p>
                                </div>
                                <div class="cont_right col-sm-4">
                                    <img src="/assets/images/productos/chapacinta/chapacinta-chapasdemadera1.jpg" alt="chapacinta">                                
                                </div>
                            </div>
                            <div class="col-md-12">
                                <img src="/assets/images/productos/chapacinta/chapacinta-lista-chapasdemadera2.jpg" alt="chapacinta lista">
                            </div>
                            <div class="tab_inn_cont_2 row">
                                <div class="cont_left col-sm-8">
                                    <h3>Chapacintas de PVC y melamina</h3>
                                    <p>Disponemos de chapacinta de la mejor calidad en diferentes espesores, medidas especiales y colores.</p>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>PVC 16mm, 19mm, 22mm, 25mm, 32mm, 35mm, 45mm, 55mm, 75mm, 100mm.</p>
                                            <ul class="two-col-list nav">
                                                <li>PVC Maple</li>
                                                <li>PVC Nogal Británico</li>
                                                <li>PVC Nogal Neo texturizado</li>
                                                <li>PVC Arce</li>
                                                <li>PVC Negro Liso</li>
                                                <li>PVC Negro</li>
                                                <li>PVC Negro Alto Brillo</li>
                                                <li>PVC Gris Cálido</li>
                                                <li>PVC Gris Claro</li>
                                                <li>PVC Gris Masisa</li>
                                                <li>PVC Roble Provenzal Text</li>
                                                <li>PVC Blanco Duraplay</li>
                                                <li>PVC Lombardía Text</li>
                                                <li>PVC Sundland Text</li>
                                                <li>PVC Vision</li>
                                                <li>PVC Nebraska</li>
                                                <li>PVC Ébano Indi</li>
                                                <li>PVC Wengue</li>
                                                <li>PVC Blanco Brillante</li>
                                            </ul>
                                        </div>
                                        <div class="col-md-6">
                                            <p>Melamina 16mm, 19mm, 22mm, 25mm, 32mm, 35mm, 45mm</p>
                                            <ul class="two-col-list nav">
                                                <li>Arce Maple</li>
                                                <li>Melamina Blanco</li>
                                                <li>Wengue Masisa</li>
                                                <li>Teka Artico</li>
                                                <li>Verce Lima</li>
                                                <li>Papel Pintable</li>
                                                <li>Blanco Absoluto</li>
                                                <li>Blanco</li>
                                                <li>Roble Tabaco</li>
                                                <li>Haya</li>
                                                <li>Nogal Neo</li>
                                                <li>Nogal Británico</li>
                                                <li>Nocce Milano</li>
                                                <li>Azul Nautico</li>
                                                <li>Arena</li>
                                                <li>Asserrado Nórdico</li>
                                                <li>Caoba</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="cont_right col-sm-4">
                                    <img src="/assets/images/productos/chapacinta/chapacinta-pvc-melamina1.jpg" alt="pvc">                                
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>                     
        </div>
    </div>
</section>

<?PHP require_once("footer.php"); ?>
</body>
</html>